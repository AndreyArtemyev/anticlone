"""anticlone URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os
from django.contrib import admin
from django.urls import include, path, re_path
from django.conf.urls import url
from django.conf.urls.static import static
from django.views.static import serve
from anticlone.settings import BASE_DIR, STATICFILES_DIRS
from rest_framework import routers
import compare.urls

#, STATIC_ROOT
static_root = STATICFILES_DIRS[0]

urlpatterns = [
    url(r'^index.html?$', serve, kwargs={'path': 'index.html', 'document_root': os.path.join(BASE_DIR, static_root)}),
    url(r'^$', serve, kwargs={'path': 'index.html', 'document_root': os.path.join(BASE_DIR, static_root)}),
    #url(r'^static/', serve, kwargs={'path': 'index.html', 'document_root': os.path.join(BASE_DIR, STATIC_ROOT)}),
    path('static/<path:path>', serve, kwargs={'document_root': os.path.join(BASE_DIR, static_root)}),
    path('admin/', admin.site.urls),
    path('compare/', include(compare.urls)),
]
