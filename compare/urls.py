"""anticlone URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from compare import views

urlpatterns = [
    path('compare', views.Compare.as_view()),
    path('list-region', views.ListRegion.as_view()),
    path('result', views.ViewsResultCompare.as_view()),
    path('pdf', views.GeneratePdf.as_view()),
    path('send', views.SendEmail.as_view()),
    #path('delete', views.ListPersons.as_view()),
]
