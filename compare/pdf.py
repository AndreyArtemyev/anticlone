import os
import json
from fpdf import FPDF
from anticlone.settings import BASE_DIR

def generate_pdf(guid, results):
    print(results)
    pdf = FPDF()
    pdf.add_page()
    pdf.add_font('Arial', '', os.path.join(BASE_DIR, 'img', 'arial.ttf'), uni=True)
    pdf.add_font('Arial', 'B', os.path.join(BASE_DIR, 'img', 'arialbd.ttf'), uni=True)
    pdf.set_left_margin(4.7)
    pdf.set_auto_page_break(True, 4.7)

    pdf.set_font('Arial', 'B', 26)
    pdf.cell(0, 10, u"Результаты проверки на уникальность", ln=1, align='C')
    pdf.set_font('Arial', 'B', 20)
    pdf.cell(0, 10, u"по коду: " + guid, align='C')

    pdf.image(os.path.join(BASE_DIR, 'img', 'header.png'), 4, 34, 200)

    pdf.set_font('Arial', '', 10)

    ch = 8
    cw = [31, 55, 40, 40]

    y = 15

    pdf.ln(27)

    max_match = 0

    for item in results:
        pdf.cell(cw[0], ch, item['filename'], ln=0)
        pdf.cell(cw[1], ch, item['region']['name_region'], ln=0)
        pdf.cell(cw[3], ch, str(item['match']), ln=0)

        match = item['match']
        if match > max_match:
            max_match = match

        if match < 5:
            pdf.cell(0, ch, "Уникальный")
        elif match < 10:
            pdf.cell(0, ch, "Имеются заимствования")
        else:
            pdf.cell(0, ch, "Аналог")

        pdf.ln(ch)

    pdf.ln(30)

    pdf.set_font('Arial', 'B', 18)
    pdf.cell(36, ch, u"Результат: ", ln=0)
    if max_match < 5:
        pdf.cell(0, ch, "код уникальный")
    elif max_match < 10:
        pdf.cell(0, ch, "в коде имеются заимствования")
    else:
        pdf.cell(0, ch, "код является аналогом существующего")
    return pdf

if __name__ == '__main__':
    results = [
        {'region': {'name_region': 'Свердловская область'}, 'match': 97.4, 'filename': 'back.cpp'},
        {'region': {'name_region': 'Тамбовская область'}, 'match': 7.12, 'filename': 'boulder-runner.cpp'},
    ]
    pdf = generate_pdf('asdfqwer', results)
    print(pdf.output('tuto1.pdf', 'S'))

