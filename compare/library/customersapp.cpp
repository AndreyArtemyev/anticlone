#include <QMessageBox>
#include <stdexcept>
#include <iostream>
#include <dataroot.h>
#include <customersapp.h>

CustomersApp::CustomersApp(int argc, char **argv)
  : QApplication(argc, argv)
  , mSettings(NULL)
  , mMainWindow(NULL)
  , mData(new DataRoot)
{
  setOrganizationName("V. V. Pavluk");
  setApplicationName("Customer Database");

  mQTTranslator.load("qt_ru.qm");
  mTranslator.load("customersapp_ru.qm");
  installTranslator(&mQTTranslator);
  installTranslator(&mTranslator);

  mMainWindow = new MainWindow;
  mSettings = new QSettings;
  mainWindow()->show();
}

CustomersApp::~CustomersApp()
{
  if(mMainWindow)
  {
    delete mMainWindow;
  }
  if(mSettings)
  {
    delete mSettings;
  }
}

MainWindow *CustomersApp::mainWindow()
{
  return mMainWindow;
}

void CustomersApp::applySettings()
{
  QString dbhost = cApp->settings().value("main/settings/dbhost").toString();
  QString dbname = cApp->settings().value("main/settings/dbname").toString();
  QString dbuser = cApp->settings().value("main/settings/dbuser").toString();
  QString dbpass = cApp->settings().value("main/settings/dbpass").toString();
  data().connect(dbhost, dbuser, dbpass, dbname);
}

QSharedPointer<ACLUserAggregate> &CustomersApp::currentUser()
{
  return mCurrentUser;
}

QSettings &CustomersApp::settings()
{
  if(mSettings)
    return *mSettings;

  throw std::runtime_error("Settings has not been read yet");
}

CustomersApp *CustomersApp::instance()
{
  return (CustomersApp*)qApp;
}

bool CustomersApp::notify(QObject *rec, QEvent *ev)
{
  try
  {
    return QApplication::notify(rec, ev);
  }
  catch(std::runtime_error &e)
  {
    QMessageBox::warning(mMainWindow, QApplication::translate("CustomersApp", "Error!"), e.what(), QMessageBox::Ok, 0, 0);
    return false;
  }
  catch(std::exception &e)
  {
    QMessageBox::critical(mMainWindow, QApplication::translate("CustomersApp", "Critical!"), e.what(), QMessageBox::Ok, 0, 0);
    abort();
  }
  catch(...)
  {
    QMessageBox::critical(mMainWindow, QApplication::translate("CustomersApp", "Critical!"), 
                                       QApplication::translate("CustomersApp", "Unknown error occured!"), QMessageBox::Ok, 0, 0);
    abort();
  }
}

int CustomersApp::getPermission(QString const &tableName)
{
  if(!mCurrentUser)
  {
    return 0;
  }
  MySQLObjectSet permission = data().ACLPermissions().findObjects(Criterion(
                          new UserTablePermissionsCriterion(mCurrentUser->metaMySQLObject()->getId(mCurrentUser.data()), tableName)));
  if(permission.begin() == permission.end())
  {
    return 0;
  }

  return permission.begin()->dynamicCast<ACLPermission>()->permission();
}

bool CustomersApp::canAdd(QString const &tableName)
{
  return getPermission(tableName) & PERM_ADD;
}

bool CustomersApp::canEdit(QString const &tableName)
{
  return getPermission(tableName) & PERM_EDIT;
}

bool CustomersApp::canDelete(QString const &tableName)
{
  return getPermission(tableName) & PERM_DELETE;
}

DataRoot &CustomersApp::data()
{
  return *mData;
}
