import re
import binascii

def items(contents):
    item = ''
    inside = 0
    q = False
    for i in contents:
        if not q and i == "(":
            if inside == 0 and item != '':
                yield item
                item = ''
            inside += 1
        if not q and inside == 0 and (i == ' ' or i == '\n') and item != "":
            yield item
            item = ''
        if item != "" or i != " " and i != "\n":
            item += i
        if i == '"':
            if q and inside == 0:
                yield item
                #[1:-1]
                item = ''
            q = not q
        if not q and i == ")":
            inside -= 1
            if inside == 0:
                yield item[1:-1]
                item = ''
    if item != '':
        yield item

def remove_comments(text):
    pattern = re.compile(r'/\*.*?\*/', re.M | re.S)
    text = pattern.sub(r"", text)
    pattern = re.compile(r'//.*?$', re.M)
    text = pattern.sub(r"", text)
    pattern = re.compile('\.', re.M | re.S)
    text = pattern.sub(" . ", text)
    pattern = re.compile('->', re.M | re.S)
    text = pattern.sub(" . ", text)
    pattern = re.compile('\n\s*?\n', re.M | re.S)
    text = pattern.sub(r"\n", text)
    pattern = re.compile('\t+| +')
    text = pattern.sub(" ", text)
    pattern = re.compile(', ')
    text = pattern.sub(",", text)
    pattern = re.compile(r'^#include\s+.*?\n', re.M | re.S)
    text = pattern.sub(r"", text)
    pattern = re.compile('\s+<', re.M | re.S)
    text = pattern.sub("<", text)
    return text

def get_shingles(text, slen):
    stopwords = [
        u'for', u'while', u'if',
        u'else', u'do', u'void',
        u'class', u'private', u'public',
        u'virtual', u'override', u'struct',
        u'typedef', '<<', '>>'
    ]
    stopsyms = '!;=,-+*/'
    text = remove_comments(text)
    shingles = []
    parts = ( [x for x in [y.strip(stopsyms) for y in items(text.lower())] if x and (x not in stopwords)] )
    out = []
    for i in range(len(parts) - (slen - 1)):
        out.append(binascii.crc32(' '.join( [x for x in parts[i:i+slen]] ).encode('utf-8')))
    return out

def compare_shingles(compared, other):
    i = 0
    for shingle in compared:
        if shingle in other:
            i += 1
    return i / len(compared)

def match(one, another, context):
    shingles = get_shingles(one, context)
    other = get_shingles(another, context)
    return compare_shingles(other, shingles)

if __name__ == '__main__':
    with open('example.cpp', 'r') as f:
        text = f.read()
        with open('compared.cpp', 'r') as f1:
            other = f1.read()
            print("Match:", 100 * match(text, other, 4), '%')
