from django.contrib import admin
from django.db import models

# Create your models here.
class Source(models.Model):
    # ID created automatically
    name = models.CharField(max_length=30, null=True)
    file_link = models.CharField(max_length=256, null=True)
    region_id = models.ForeignKey('Region', on_delete=models.SET_NULL, null=True)

# Create your models here.
class ResultCompare(models.Model):
    # ID created automatically
    result = models.TextField(max_length=30, null=True)
    guid = models.CharField(max_length=64, null=False)

# Create your models here.
class Region(models.Model):
    # ID created automatically
    name_region = models.CharField(max_length=30, null=True)

admin.site.register(Source)
admin.site.register(ResultCompare)
admin.site.register(Region)

