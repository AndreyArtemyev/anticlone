import smtplib
from decouple import config
from email.message import EmailMessage
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, FileResponse
from compare import models
from compare import serializers
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.parsers import JSONParser
from compare import Shingles
from threading import Thread
from anticlone.settings import BASE_DIR
from compare import pdf
import json
from django.views.decorators.clickjacking import xframe_options_exempt
import os
import uuid

def process_compare_file(data, result, sources):
    res = {}
    match_max = {}
    match_by_region = []
    serializer = serializers.RegionSerializer

    for source in sources:
        match_region = source.region_id.pk
        with open(os.path.join(BASE_DIR,source.file_link), "r") as f:
            comparand = f.read()
            match_value = Shingles.match(data, comparand, 4)
            if match_value > 0:
                match_by_region.append({
                    'region': serializer(source.region_id).data,
                    'filename': source.name,
                    'match': round(match_value * 100, 2),
                })
            #if match_value > match_by_region.get(match_region, 0):
            #    match_by_region[match_region] = match_value

    match_by_region.sort(reverse=True, key=lambda x: x['match'])
    res['shingles'] = match_by_region
    result.result = json.dumps(res)
    result.save()

def makeClosure(*args):
    def process_fn():
        process_compare_file(*args)
    return process_fn

# Create your views here.
class Compare(APIView):

    @xframe_options_exempt
    def post(self, request, format=None):
        """
        Compare and create a result
        """
        result = {
            'error': False,
            'result': None,
        }
        source = request.FILES.get("source")
        #regions = request.POST["regions"]

        if source is None:
            result['error'] = True
            result['result'] = 'No file!'
        elif source.multiple_chunks():
            result['error'] = True
            result['result'] = 'File is too large!'
        else:
            res = models.ResultCompare()
            res.guid = uuid.uuid1()
            res.save()
            result['result'] = res.guid
            compared = source.read().decode('UTF-8')
            thread = Thread(target = makeClosure(compared, res, models.Source.objects.all()))
            thread.daemon = True
            thread.start()

        return JsonResponse(result, safe=False)

class GetResult(APIView):

    def post(self, request, format=None):
        """
        Get the result
        """
        data = JSONParser().parse(request)
        serializer = serializers.PersonSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

# Create your views here.
class ListRegion(APIView):

    @xframe_options_exempt
    def get(self, request, format=None):
        """
        Return a list of all persons.
        """
        serializer = serializers.RegionSerializer()
        regions = [serializers.RegionSerializer(region).data for region in models.Region.objects.all()]
        return JsonResponse(regions, safe=False)

# Create your views here.
class ViewsResultCompare(APIView):

    @xframe_options_exempt
    def get(self, request, format=None):
        """
        Return a list of all persons.
        """
        id = request.GET['id']
        result = None
        try:
          result = models.ResultCompare.objects.get(guid=id.lower())
        except:
          JsonResponse({"error": True, "result": "No such result"})
        serializer = serializers.ResultCompareSerializer
        return JsonResponse(serializer(result).data, safe=False)

# Create your views here.
class GeneratePdf(APIView):

    def get(self, request, format=None):
        """
        Return a list of all persons.
        """
        id = request.GET['id']
        result = models.ResultCompare.objects.get(guid=id.lower()).result
        shingles = json.loads(result)
        doc = pdf.generate_pdf(id, shingles['shingles'])
        buf = doc.output('result.pdf', 'S')
        path = os.path.join(BASE_DIR, 'tmp', id + '.pdf')
        doc.output(path, 'F')
        response = FileResponse(open(path, 'rb'), content_type='application/pdf', as_attachment=True, filename='result.pdf')
        return response

class SendEmail(APIView):

    def get(self, request, format=None):
        id = request.GET['id']
        msg = EmailMessage()
        content = """
Вам прислали ссылку на результаты проверки кода на уникальность

Код: {}

Ссылка: {}
        """
        url = request.build_absolute_uri('?')
        url = url.rsplit('/', 1)[0]
        url = request.GET.get('base', url)
        c = content.format(id, url + '/result?id=' + id)
        msg.set_content(c)
        msg['Subject'] = 'Ссылка на результаты проверки'
        msg['From'] = 'noreply@anticlone.org'
        msg['To'] = request.GET.get('to')

        s = smtplib.SMTP(config('EMAIL_RELAY'))
        s.login(config('EMAIL_LOGIN'), config('EMAIL_PASS'))
        s.send_message()
        s.quit()

        return JsonResponse({"result": "ok", "msg": c}, safe=False)

