# Generated by Django 2.2.5 on 2019-09-28 09:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('compare', '0004_auto_20190928_0857'),
    ]

    operations = [
        migrations.AddField(
            model_name='resultcompare',
            name='guid',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
    ]
