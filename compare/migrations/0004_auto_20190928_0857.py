# Generated by Django 2.2.5 on 2019-09-28 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('compare', '0003_auto_20190927_1915'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resultcompare',
            name='source_id',
        ),
        migrations.AlterField(
            model_name='source',
            name='file_link',
            field=models.CharField(max_length=256, null=True),
        ),
    ]
