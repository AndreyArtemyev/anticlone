from rest_framework import serializers
from compare import models

class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Source
        fields = '__all__'

class ResultCompareSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ResultCompare
        fields = '__all__'

class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Region
        fields = '__all__'